public class Traveller {
    public int baggageAmount;
    public int immigrationCardValidity;
    public boolean nocStatus;

    public Traveller(int baggageAmount,int immigrationCardValidity,boolean nocStatus) {
        this.baggageAmount = baggageAmount;
        this.immigrationCardValidity=immigrationCardValidity;
        this.nocStatus=nocStatus;
    }
}
