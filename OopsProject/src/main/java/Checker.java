public class Checker {
    boolean checkBaggage(int amount){
        return amount >= 0 && amount <= 40;
    }
    boolean checkImmigration(int immigrationCardValidityYear){
        return immigrationCardValidityYear >= 2021 && immigrationCardValidityYear <= 2025;
    }
    boolean checkNocStatus(boolean nocStatus){
        return nocStatus;
    }
    boolean checkEverything(Traveller traveller){
        if((checkBaggage(traveller.baggageAmount) && checkImmigration(traveller.immigrationCardValidity) && checkNocStatus(traveller.nocStatus))){
            return true;
        }
        else return false;
    }

}
