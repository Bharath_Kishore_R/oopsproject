import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TravellerTest {
    Checker checker = new Checker();

    @Test
    void shouldReturnTrueIfGivenDetailsOfTravellerIsValid() {
        Traveller traveller = new Traveller(35, 2022, true);
        assertTrue(checker.checkEverything(traveller), "Allow traveller to Fly");

    }
    @Test
    void shouldReturnFalseIfBaggageAmountOfTravellerIsInvalid(){
        Traveller traveller=new Traveller(45,2023,true);
        assertFalse(checker.checkEverything(traveller),"Don't allow traveller to fly");
    }
}
